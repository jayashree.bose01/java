FROM java:8
RUN mkdir -p /java_f
WORKDIR /java_f
COPY target/spring-boot-2-rest-service-basic-0.0.1-SNAPSHOT-jar-with-dependencies.jar /java_f/spring-boot-2-rest-service-basic-0.0.1-SNAPSHOT-jar-with-dependencies.jar
CMD java -jar spring-boot-2-rest-service-basic-0.0.1-SNAPSHOT-jar-with-dependencies.jar
