#!/bin/sh

getrelease=$(helm ls springbootapp --tiller-namespace java | grep springbootapp | awk '{print $1}')

if [ $getrelease == springbootapp ]
then
(
helm delete --purge springbootapp --tiller-namespace java
)
fi
